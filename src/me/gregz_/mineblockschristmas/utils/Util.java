package me.gregz_.mineblockschristmas.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.gregz_.mineblockschristmas.chat.Logger;
import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import me.gregz_.mineblockschristmas.commands.permission.Permission;

public class Util {

	public static boolean debug = false;

	// ITEMSTACK

	@SuppressWarnings("deprecation")
	public static ItemStack parseItemStack(String s) {
		try {
			String[] gSplit = s.split(" ");
			ItemStack is = null;

			// [(ITEM ID / MATERIAL) / SUBID]
			String[] idsSplit = gSplit[0].split(":");
			try {
				is = new ItemStack(Integer.parseInt(idsSplit[0]));
			} catch (NumberFormatException e) {
				is = new ItemStack(Material.valueOf(idsSplit[0]));
			}

			if (idsSplit.length > 1)
				is.setDurability(Short.parseShort(idsSplit[1]));

			if (gSplit.length > 1) {
				int metaStart = 2;

				try {
					is.setAmount(Integer.parseInt(gSplit[1]));
				} catch (NumberFormatException e) {
					metaStart = 1;
				}
				ItemMeta im = is.getItemMeta();
				for (int meta = metaStart; meta < gSplit.length; meta++) {
					String rawKey = gSplit[meta];
					String[] split = rawKey.split(":");
					String key = split[0];

					if (key.equalsIgnoreCase("name")) {
						im.setDisplayName(ChatColor.translateAlternateColorCodes('&', split[1]).replace("_", " "));
					} else if (key.equalsIgnoreCase("lore")) {
						List<String> lore = new ArrayList<>();
						for (String line : split[1].split("//")) {
							lore.add(ChatColor.translateAlternateColorCodes('&', line).replace("_", " "));
						}
						im.setLore(lore);
					}
				}
				is.setItemMeta(im);
			}

			return is;
		} catch (Exception e) {
			Logger.error("Cannot parse ItemStack: " + s + " - Mabye this is the reason: " + e.toString());
			return null;
		}
	}

	// TIME

	public static String getFormatedTime(long seconds) {
		int minutes = (int) seconds / 60;
		int hours = minutes / 60;
		int days = hours / 24;

		seconds -= minutes * 60;
		minutes -= hours * 60;
		hours -= days * 24;

		StringBuilder sb = new StringBuilder();
		if (days > 0)
			sb.append(days).append(" days ");
		if (hours > 0)
			sb.append(hours).append(" hours ");
		if (minutes > 0)
			sb.append(minutes).append(" minutes ");
		if (seconds > 0)
			sb.append(seconds).append(" seconds ");

		return sb.toString().substring(0, sb.toString().length() - 1);
	}

	// LOCATION

	public static Location parseLocation(String s) {
		String[] split = s.split(",");
		Location loc = null;

		try {
			World world = Bukkit.getWorld(split[0]);
			if (split.length == 6) {
				double x = Double.parseDouble(split[1]);
				double y = Double.parseDouble(split[2]);
				double z = Double.parseDouble(split[3]);

				float yaw = Float.parseFloat(split[4]);
				float pitch = Float.parseFloat(split[5]);
				loc = new Location(world, x, y, z, yaw, pitch);
			} else if (split.length == 4) {
				int x = Integer.parseInt(split[1]);
				int y = Integer.parseInt(split[2]);
				int z = Integer.parseInt(split[3]);

				loc = new Location(world, x, y, z);
			}
		} catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
			Logger.error("Cannot parse location from string: " + s);
		}

		return loc;
	}

	public static String serializeLocation(Location l, boolean exact) {
		if (l != null) {
			StringBuilder key = new StringBuilder();
			key.append(l.getWorld().getName() + ",");
			if (exact) {
				key.append(l.getX() + "," + l.getY() + "," + l.getZ() + "," + l.getYaw() + "," + l.getPitch());
			} else {
				key.append(l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ());
			}

			return key.toString();
		}
		return null;
	}

	// DEBUG

	public static void debug(Object object) {
		if (debug) {
			Logger.debug(object.toString());
			for (Player p : Bukkit.getOnlinePlayers()) {
				if (p.isOp() || p.hasPermission(Permission.DEBUG.getPermission())) {
					p.sendMessage(MessageHandler.getMessage("prefix") + "�r [�2DEBUG�8]�r �a" + object.toString());
				}
			}
		}
	}
}
