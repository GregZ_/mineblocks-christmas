package me.gregz_.mineblockschristmas.broadcasts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.gregz_.mineblockschristmas.MineblocksChristmas;
import me.gregz_.mineblockschristmas.chat.Logger;
import me.gregz_.mineblockschristmas.utils.Util;

public class BroadcastManager {
	private ArrayList<String> broadcasts = new ArrayList<>();
	private String prefix;
	private int delay;
	private Random r = new Random();
	private BukkitRunnable broadcastTask;
	private boolean running;
	private List<World> worldsToBroadcastTo;

	public BroadcastManager() {
		reload();
	}

	public void reload() {
		stopBroadcastTask();
		broadcasts.clear();
		if (!(MineblocksChristmas.broadcasts.contains("Broadcasts"))) {
			Logger.error("No broadcasts configuration section found in the broadcasts.yml");
			return;
		}
		for (String key : MineblocksChristmas.broadcasts.getStringList("Broadcasts")) {
			broadcasts.add(replaceColors(key));
		}

		prefix = replaceColors(MineblocksChristmas.broadcasts.getString("prefix", "&c&lSanta &8� "));
		delay = MineblocksChristmas.broadcasts.getInt("delay", 300);

		startBroadcastTask();

		Logger.log(broadcasts.size() + (broadcasts.size() == 1 ? " broadcast message" : " broadcast messages") + " loaded!");
	}

	public void startBroadcastTask() {
		if (running) {
			return;
		}
		running = true;
		broadcastTask = new BukkitRunnable() {
			String message;

			@Override
			public void run() {
				message = getRandomBroadcast();
				for (Player p : Bukkit.getOnlinePlayers()) {
					if (!(worldsToBroadcastTo.isEmpty())) {
						for (Player p1 : Bukkit.getOnlinePlayers()) {
							p1.sendMessage(message);
						}
						Logger.error("As the WorldsToBroadcastTo in the broadcasts.yml was empty the broadcast has been sent to all online players!");
						return;
					}
					for (World w : worldsToBroadcastTo) {
						if (p.getWorld() == w) {
							p.sendMessage(message);
						}
					}
				}

				Util.debug("Sent broadcast message: " + message);

				if (!(running)) {
					this.cancel();
				}
			}
		};
		broadcastTask.runTaskTimer(MineblocksChristmas.instance, 200, delay * 20);
	}

	public boolean stopBroadcastTask() {
		if (running) {
			broadcastTask.cancel();
			running = false;
			return true;
		}
		return false;
	}

	public String getRandomBroadcast() {
		if (!(broadcasts.isEmpty())) {
			return prefix + broadcasts.get(r.nextInt(broadcasts.size()));
		} else {
			return "�cNo broadcasts found!";
		}
	}

	public String replaceColors(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

	public List<String> getBroadcasts() {
		List<String> broadcastsList = new ArrayList<>();
		if (broadcasts.isEmpty()) {
			broadcastsList.add("�cNo broadcasts are loaded!");
			return broadcastsList;
		}
		for (String s : broadcasts) {
			broadcastsList.add("�7- �6" + s + ":\n");
		}
		return broadcastsList;
	}
}
