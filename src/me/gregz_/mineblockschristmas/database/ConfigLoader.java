package me.gregz_.mineblockschristmas.database;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import me.gregz_.mineblockschristmas.MineblocksChristmas;

public class ConfigLoader {

	public void load() {
		reloadConfig();
		reloadMessages();
		reloadDatabase();
		reloadBroadcasts();
		reloadParticleEffects();
	}
	
	public static void reloadConfig() {
		MineblocksChristmas.instance.reloadConfig();
		FileConfiguration c = MineblocksChristmas.instance.getConfig();
		
		c.addDefault("use-permissions", true);
		c.addDefault("UseColourInConsole", true);

		c.addDefault("Defaults.Snow.Amount.Min", 1250);
		c.addDefault("Defaults.Snow.Amount.Max", 1500);
		c.addDefault("Defaults.Snow.Range", 100);

		c.options().copyDefaults(true);
		MineblocksChristmas.instance.saveConfig();
	}
	
	public static void reloadMessages() {
		FileConfiguration c = new YMLLoader(MineblocksChristmas.instance.getDataFolder().toString(), "messages.yml").getFileConfiguration();
		MineblocksChristmas.messages = c;
		
		c.addDefault("prefix", "&8[&3Mineblocks Christmas&8] &6");
		c.addDefault("no-permission", "&cYou don't have permission to do this!");
		c.addDefault("cmd-error", "&cError: %0%");
		
		c.addDefault("snowhub-already-exists", "&cThe snow hub %0% already exist!");
		c.addDefault("snowhub-not-found", "&cThe snow hub %0% does not exists!");
		c.addDefault("snowhub-must-enter", "&cYou must enter a name: %0%");
		c.addDefault("snowhub-created", "You've created the snow hub %0% successfully!");
		c.addDefault("snowhub-move-success", "You've successfully moved the snow hub %0%");
		c.addDefault("snowhub-reload-success", "You've successfully reloaded the snow hub %0%");

		c.addDefault("effect-on-cooldown", "&cYou cant currently use an effect you have %0% left until you may next use an effect!");
		c.addDefault("effect-not-selected", "&cYou have not selected an effect yet! Please right click this item to select an effect");
		
		c.addDefault("config-error-name", "&cPlease enter a valid configuration name: %0%");

		c.options().copyDefaults(true);
		MineblocksChristmas.saveMessages();
	}
	
	public static void reloadBroadcasts() {
		FileConfiguration c = new YMLLoader(MineblocksChristmas.instance.getDataFolder().toString(), "broadcasts.yml").getFileConfiguration();
		MineblocksChristmas.broadcasts = c;
		
		c.addDefault("prefix", "&c&lSanta&r &8� ");
		c.addDefault("delay", 300);
		c.addDefault("AllWorlds", true);
		c.addDefault("WorldsToBroadcastTo", new ArrayList<>());
		
		List<String> broadcasts = new ArrayList<>();
		broadcasts.add("&6Ho ho ho, Merry Christmas");
		broadcasts.add("&6Hope your Christmas is a perfect measure of fun and laughter");
		c.addDefault("Broadcasts", broadcasts);

		c.options().copyDefaults(true);
		MineblocksChristmas.saveBroadcasts();
	}
	
	public static void reloadDatabase() {
		FileConfiguration c = new YMLLoader(MineblocksChristmas.instance.getDataFolder().toString(), "database.yml").getFileConfiguration();
		MineblocksChristmas.database = c;
	}
	
	public static void reloadParticleEffects() {
		FileConfiguration c = new YMLLoader(MineblocksChristmas.instance.getDataFolder().toString(), "particleeffects.yml").getFileConfiguration();
		MineblocksChristmas.particleeffects = c;
		
		c.addDefault("InventoryTitle", "&dPartical Effects!");
		c.addDefault("Cooldown.Standard", 20);
		c.addDefault("Cooldown.Short", 10);
		
		List<String> noPermissionLore = new ArrayList<>();
		noPermissionLore.add("&cYou do not have the required permission to use this effect!");
		c.addDefault("NoPermissionLore", noPermissionLore);
		
		c.addDefault("Selector.Item", "BLAZE_ROD 1 name:&5&k12&r&d_Effect_Selector_&5&k12 lore:&3&k12&r&b_Right_Click_to_select_an_effect_to_use!_&3&k12");
		c.addDefault("Selector.Slot", 0);
		
		c.addDefault("SelectorItemNames.WINDOFBLOODEFFECT", "&cWind Of Blood");
		c.addDefault("SelectorItemNames.MAGICWAVE", "&dMagic Wave");
		c.addDefault("SelectorItemNames.FIREWAVE", "&6Fire Wave");
		c.addDefault("SelectorItemNames.WATERWAVE", "&9Water Wave");
		c.addDefault("SelectorItemNames.SPARKCANNON", "&3Spark Cannon");
		c.addDefault("SelectorItemNames.LAVAPOPCANNON", "&4&K12&r &cLava Pop Cannon &4&K12");
		c.addDefault("SelectorItemNames.RANDOMWAVE", "&4&K12&r &cWave of Random &4&K12");
		c.addDefault("SelectorItemNames.FIREBALLCANNON", "&6Fire Ball Cannon");
		c.addDefault("SelectorItemNames.HELIXOFRANDOM", "&4&K12&r &cHelix of Random &4&K12");
		
		c.options().header("## How do I format the items? ##\n"
				+ "MATERIAL/ITEMID[:SUBID] [AMOUNT] [SPECIAL THINGS]\n"
				+ "Here are some examples:\n"
				+ "\n"
				+ "# Normal Item:\n"
				+ "\"BREAD\" - is the same like \"BREAD 1\", \"BREAD:0 1\" or \"297:0 1\"\n"
				+ "\n"
				+ "# If you want to set a predefined durability-level, just change the subid:\n"
				+ "\"STONE_SWORD:10\" - This tool has already 10 uses lost.\n"
				+ "\n"
				+ "# You can also set a custom name and lore for an item:\n"
				+ "\"EGG name:&eEaster_Egg lore:&7Throw//&7me!\" - This is an egg with a displayname \"Easter Egg\" and the lore \"Throw me\"! Note: Spaces are \"_\" and line breaks in lore are the characters \"//\"\n");
		
		c.options().copyDefaults(true);
		MineblocksChristmas.saveParticleEffects();
	}
}
