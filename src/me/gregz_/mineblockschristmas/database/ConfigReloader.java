package me.gregz_.mineblockschristmas.database;

import me.gregz_.mineblockschristmas.MineblocksChristmas;
import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import me.gregz_.mineblockschristmas.commands.permission.PermissionHandler;

public class ConfigReloader {
	public static void reloadConfig() {
		ConfigLoader.reloadConfig();
		PermissionHandler.reinitializeUsePermission();
		MineblocksChristmas.snowHubManager.reinitializeConfig();
	}

	public static void reloadDatabase() {
		ConfigLoader.reloadDatabase();
		MineblocksChristmas.snowHubManager.reinitializeDatabase();
	}

	public static void reloadMessage() {
		ConfigLoader.reloadMessages();
		MessageHandler.reload();
	}
	
	public static void reloadBroadcasts() {
		ConfigLoader.reloadBroadcasts();
		MineblocksChristmas.broadcastManager.reload();
	}
	
	public static void reloadParticleEffects() {
		ConfigLoader.reloadParticleEffects();
		MineblocksChristmas.particalEffectInventory.reload();
	}
}
