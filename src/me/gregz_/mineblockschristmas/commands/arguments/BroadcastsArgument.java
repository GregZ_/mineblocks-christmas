package me.gregz_.mineblockschristmas.commands.arguments;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.gregz_.mineblockschristmas.MineblocksChristmas;
import me.gregz_.mineblockschristmas.chat.Helper;
import me.gregz_.mineblockschristmas.chat.Logger;
import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import me.gregz_.mineblockschristmas.commands.permission.Permission;
import me.gregz_.mineblockschristmas.commands.permission.PermissionHandler;

public class BroadcastsArgument {

	private CommandSender sender;
	private String[] args;

	public BroadcastsArgument(CommandSender sender, String[] args) {
		this.sender = sender;
		this.args = args;
	}

	public boolean execute() {
		if (!(sender instanceof Player)) {
			sender.sendMessage("�cThe broadcasts argument can only execute as a Player!");
			return true;
		}

		Player p = (Player) sender;

		if (!PermissionHandler.hasPermission(p, Permission.BROADCASTS)) {
			p.sendMessage(MessageHandler.getMessage("no-permission"));
			Logger.log(sender.getName() + " Was denied access to /mbc broadcasts");
			return true;
		}

		if (args.length == 1) {
			Helper.showConfigHelpsite(p);
		} else {
			if (args[1].equalsIgnoreCase("list")) {
				p.sendMessage(MessageHandler.getMessage("prefix") + "Broadcasts �7�m---�r �6List of loaded broadcasts");
				for (String s : MineblocksChristmas.broadcastManager.getBroadcasts()) {
					p.sendMessage(s);
				}
			}
		}
		p.sendMessage(MessageHandler.getMessage("prefix") + "�cCommand not found! Type /mbc broadcasts for help!");
		return true;
	}
}
