package me.gregz_.mineblockschristmas.commands.arguments;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.gregz_.mineblockschristmas.chat.Helper;
import me.gregz_.mineblockschristmas.chat.Logger;
import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import me.gregz_.mineblockschristmas.commands.permission.Permission;
import me.gregz_.mineblockschristmas.commands.permission.PermissionHandler;
import me.gregz_.mineblockschristmas.database.ConfigReloader;

public class ConfigArgument {

	private CommandSender sender;
	private String[] args;

	public ConfigArgument(CommandSender sender, String[] args) {
		this.sender = sender;
		this.args = args;
	}

	public boolean execute() {
		if (!(sender instanceof Player)) {
			sender.sendMessage("�cThe config argument can only execute as a Player!");
			return true;
		}

		Player p = (Player) sender;

		if (!PermissionHandler.hasPermission(p, Permission.CONFIG)) {
			p.sendMessage(MessageHandler.getMessage("no-permission"));
			Logger.log(sender.getName() + " Was denied access to /mbc config");
			return true;
		}

		if (args.length == 1) {
			Helper.showConfigHelpsite(p);
		} else {
			if (args[1].equalsIgnoreCase("reload")) {
				if (args.length == 2) {
					ConfigReloader.reloadMessage();
					ConfigReloader.reloadConfig();
					ConfigReloader.reloadDatabase();
					ConfigReloader.reloadBroadcasts();
					ConfigReloader.reloadParticleEffects();
					p.sendMessage(MessageHandler.getMessage("prefix") + "You've reloaded all configuration files successfully!");
					return true;
				}

				String con = args[2];

				if (con.equalsIgnoreCase("messages")) {
					ConfigReloader.reloadMessage();
					p.sendMessage(MessageHandler.getMessage("prefix") + "You've reloaded the messages.yml successfully!");
				}

				else if (con.equalsIgnoreCase("database")) {
					ConfigReloader.reloadDatabase();
					p.sendMessage(MessageHandler.getMessage("prefix") + "The settings are applied to a lobby after a lobby-reload or the end of a survival game.");
				}

				else if (con.equalsIgnoreCase("config")) {
					ConfigReloader.reloadConfig();
					p.sendMessage(MessageHandler.getMessage("prefix") + "You've reloaded the config.yml successfully!");
				}
				
				else if (con.equalsIgnoreCase("broadcasts")) {
					ConfigReloader.reloadBroadcasts();
					p.sendMessage(MessageHandler.getMessage("prefix") + "You've reloaded the broadcasts.yml successfully!");
				}
				
				else if (con.equalsIgnoreCase("particleeffects")) {
					ConfigReloader.reloadParticleEffects();
					p.sendMessage(MessageHandler.getMessage("prefix") + "You've reloaded the particleeffects.yml successfully!");
				}

				else {
					p.sendMessage(MessageHandler.getMessage("config-error-name").replace("%0%", "/mbc config reload [MESSAGES/DATABASE/CONFIG/BROADCASTS/PARTICLEEFFECTS]"));
					return true;
				}

			}
		}
		return true;
	}

}
