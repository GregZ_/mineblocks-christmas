package me.gregz_.mineblockschristmas.commands.arguments;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.gregz_.mineblockschristmas.MineblocksChristmas;
import me.gregz_.mineblockschristmas.chat.Helper;
import me.gregz_.mineblockschristmas.chat.Logger;
import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import me.gregz_.mineblockschristmas.commands.permission.Permission;
import me.gregz_.mineblockschristmas.commands.permission.PermissionHandler;

public class SnowArgument {

	private CommandSender sender;
	private String[] args;

	public SnowArgument(CommandSender sender, String[] args) {
		this.sender = sender;
		this.args = args;
	}

	public boolean execute() {
		if (!PermissionHandler.hasPermission(sender, Permission.SETSNOWHUB)) {
			sender.sendMessage(MessageHandler.getMessage("no-permission"));
			Logger.log(sender.getName() + " Was denied access to /mbc snow");
			return true;
		}

		if (!(sender instanceof Player)) {
			sender.sendMessage(MessageHandler.getMessage("prefix") + "�cThe snow argument can only execute as a Player!");
			return true;
		}

		Player p = (Player) sender;

		if (args.length == 1) {
			Helper.showSnowHelpsite(p);
			return true;
		} else {
			if (args[1].equalsIgnoreCase("add")) {
				if (args.length == 2) {
					p.sendMessage(MessageHandler.getMessage("snowhub-must-enter").replace("%0%", "/mcb snow add <SNOW HUB NAME>"));
					return true;
				}
				MineblocksChristmas.snowHubManager.add(p, getArgs(2));
				return true;
			} else if (args[1].equalsIgnoreCase("delete")) {
				if (args.length == 2) {
					p.sendMessage(MessageHandler.getMessage("snowhub-must-enter").replace("%0%", "/mcb snow delete <SNOW HUB NAME>"));
					return true;
				}
				MineblocksChristmas.snowHubManager.delete(p, getArgs(2));
				return true;
			} else if (args[1].equalsIgnoreCase("move")) {
				if (args.length == 2) {
					p.sendMessage(MessageHandler.getMessage("snowhub-must-enter").replace("%0%", "/mcb snow move <SNOW HUB NAME>"));
					return true;
				}
				MineblocksChristmas.snowHubManager.move(p, getArgs(2));
				return true;
			} else if (args[1].equalsIgnoreCase("reload")) {
				if (args.length == 2) {
					p.sendMessage(MessageHandler.getMessage("snowhub-must-enter").replace("%0%", "/mcb snow reload <SNOW HUB NAME>"));
					return true;
				}
				MineblocksChristmas.snowHubManager.reload(p, getArgs(2));
				return true;
			} else if (args[1].equalsIgnoreCase("reloadall")) {
				MineblocksChristmas.snowHubManager.reloadAll(p);
				return true;
			} else if (args[1].equalsIgnoreCase("load")) {
				if (args.length == 2) {
					p.sendMessage(MessageHandler.getMessage("snowhub-must-enter").replace("%0%", "/mcb snow load <SNOW HUB NAME>"));
					return true;
				}
				if (!(MineblocksChristmas.snowHubManager.load(getArgs(2)))) {
					p.sendMessage(MessageHandler.getMessage("prefix") + "�cError loading snow hub " + getArgs(2) + " please view the console for details!");
				}
				return true;
			} else if (args[1].equalsIgnoreCase("unload")) {
				if (args.length == 2) {
					p.sendMessage(MessageHandler.getMessage("snowhub-must-enter").replace("%0%", "/mcb snow unload <SNOW HUB NAME>"));
					return true;
				}
				if (!(MineblocksChristmas.snowHubManager.unload(getArgs(2)))) {
					p.sendMessage(MessageHandler.getMessage("prefix") + "�cError unloading snow hub " + getArgs(2) + " please view the console for details!");
				}
				return true;
			} else if (args[1].equalsIgnoreCase("list")) {
				p.sendMessage(MessageHandler.getMessage("prefix") + "Snow Hubs �7�m---�r �6List of loaded snow hubs");
				for (String s : MineblocksChristmas.snowHubManager.getSnowHubs()) {
					p.sendMessage(s);
				}
				return true;
			}
		}
		p.sendMessage(MessageHandler.getMessage("prefix") + "�cCommand not found! Type /mbc snow for help!");
		return true;
	}

	private String getArgs(int i) {
		StringBuilder sb = new StringBuilder();
		for (int a = i; i < args.length; a++) {
			try {
				sb.append(args[a] + "_");
			} catch (ArrayIndexOutOfBoundsException e) {
				break;
			}
		}
		return sb.toString().substring(0, sb.toString().length() - 1);
	}
}
