package me.gregz_.mineblockschristmas.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.gregz_.mineblockschristmas.MineblocksChristmas;
import me.gregz_.mineblockschristmas.commands.arguments.BroadcastsArgument;
import me.gregz_.mineblockschristmas.commands.arguments.ConfigArgument;
import me.gregz_.mineblockschristmas.commands.arguments.SnowArgument;
import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import me.gregz_.mineblockschristmas.commands.permission.Permission;
import me.gregz_.mineblockschristmas.commands.permission.PermissionHandler;
import me.gregz_.mineblockschristmas.utils.Util;

public class CommandMBC implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (cmd.getName().equalsIgnoreCase("mbc")) {

			if (args.length == 0) {
				sender.sendMessage(MessageHandler.getMessage("prefix") + MineblocksChristmas.instance.getDescription().getName() + " Version " + MineblocksChristmas.instance.getDescription().getVersion() + " �7�m--�r �ePlugin developed by GregZ_");

				if (PermissionHandler.hasPermission(sender, Permission.SETSNOWHUB)) {
					sender.sendMessage("�8/�6mbc snow �7- �eShows the snow helpsite!");
				}
				if (PermissionHandler.hasPermission(sender, Permission.CONFIG)) {
					sender.sendMessage("�8/�6mbc config �7- �eShows the configuration management helpsite!");
				}
				if (PermissionHandler.hasPermission(sender, Permission.BROADCASTS)) {
					sender.sendMessage("�8/�6mbc broadcasts �7- �eShows the broadcast management helpsite!");
				}
				if (PermissionHandler.hasPermission(sender, Permission.DEBUG)) {
					sender.sendMessage("�8/�6mbc debug �7- �eToggles the built in debug mode!");
				}
			} else {
				if (args[0].equalsIgnoreCase("snow")) {
					return new SnowArgument(sender, args).execute();
				} else if (args[0].equalsIgnoreCase("config")) {
					return new ConfigArgument(sender, args).execute();
				} else if (args[0].equalsIgnoreCase("broadcasts")) {
					return new BroadcastsArgument(sender, args).execute();
				}

				if (args[0].equalsIgnoreCase("debug")) {
					if (!PermissionHandler.hasPermission(sender, Permission.DEBUG)) {
						sender.sendMessage(MessageHandler.getMessage("no-permission"));
						return true;
					}
					boolean nV = !Util.debug;
					Util.debug = nV;
					sender.sendMessage(MessageHandler.getMessage("prefix") + "Debug Mode�7: " + (nV ? "�aENABLED" : "�cDISABLED"));
					return true;
				}
			}
		}
		return false;
	}

}
