package me.gregz_.mineblockschristmas.commands.permission;

public enum Permission {
	WINDOFBLOODEFFECT("mbc.effect.windofblood"), // Allows players to use the wind of blood effect.
	MAGICWAVE("mbc.effect.magicwave"), // Allows players to use the magic effect.
	FIREWAVE("mbc.effect.firewave"), // Allows players to use the fire wave effect.
	WATERWAVE("mbc.effect.waterwave"), // Allows players to use the water wave effect.
	SPARKCANNON("mbc.effect.sparkcannon"), // Allows players to use the spark cannon effect.
	LAVAPOPCANNON("mbc.effect.lavapopcannon"), // Allows players to use the lava pop cannon effect.
	RANDOMWAVE("mbc.effect.randomwave"), // Allows players to use the power wave effect.
	FIREBALLCANNON("mbc.effect.fireballcannon"), // Allows players to use the Christmas Wave effect.
	HELIXOFPOWER("mbc.effect.helixofpower"), // Allows players to use the helix of power effect.

	USEALLEFFECTS("mbc.effect.all"), // Allows players to use all effects.

	SHORTEFFECTCOOLDOWN("mbc.shorteffectcooldown"), // Allows the player to have a shorter cooldown on effects.
	BYPASSEFFECTCOOLDOWN("mbc.bypasseffectcooldown"), // Allows the player to have no cooldown on effects.
	DROPWANDITEM("mbc.dropwand"), // Allows players to drop the particle effect wand if they are sneaking.

	CONFIG("mbc.admin.config"), // Allows the player to reload configuration files with /mbc config reload [config to be reloaded] and to see the config help site.

	DEBUG("mbc.debug"), // Allows the player access to the debug command (nobody should need this).

	SETSNOWHUB("mbc.snowhub"), // Allows access to /mbc snow.

	BROADCASTS("mbc.admin.broadcasts"); //Allows players access to /mbc broadcasts.

	private final String permission;

	private Permission(String permission) {
		this.permission = permission;
	}

	public String getPermission() {
		return permission;
	}
}
