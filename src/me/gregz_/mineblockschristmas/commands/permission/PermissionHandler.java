package me.gregz_.mineblockschristmas.commands.permission;

import org.bukkit.command.CommandSender;

import me.gregz_.mineblockschristmas.MineblocksChristmas;

public class PermissionHandler {

	private static boolean usePermission = MineblocksChristmas.instance.getConfig().getBoolean("use-permissions");

	public static void reinitializeUsePermission() {
		usePermission = MineblocksChristmas.instance.getConfig().getBoolean("use-permissions");
	}

	public static boolean hasPermission(CommandSender sender, Permission permission) {
		if (usePermission) {
			return sender.hasPermission(permission.getPermission());
		} 
		else {
			if (sender.isOp()) {
				return true;
			} else {
				return permission == Permission.SHORTEFFECTCOOLDOWN || permission == Permission.USEALLEFFECTS;
			}
		}
	}
}
