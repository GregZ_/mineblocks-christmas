package me.gregz_.mineblockschristmas.commands.messages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;

import me.gregz_.mineblockschristmas.MineblocksChristmas;
import me.gregz_.mineblockschristmas.chat.Logger;

public class MessageHandler {

	private static HashMap<String, String> messages = new HashMap<>();
	private static List<String> withoutPrefix = new ArrayList<>();

	public static void reload() {
		messages.clear();
		for (String key : MineblocksChristmas.messages.getConfigurationSection("").getKeys(false)) {
			messages.put(key, replaceColors(MineblocksChristmas.messages.getString(key)));
		}

		withoutPrefix.add("prefix");

		Logger.log(messages.size() + (messages.size() == 1 ? " message" : " messages") + " loaded!");
	}

	public static String getMessage(String name) {
		if (messages.containsKey(name)) {
			if (withoutPrefix.contains(name)) {
				return messages.get(name);
			} else {
				return messages.get("prefix") + messages.get(name);
			}
		} else {
			return "�cMessage not found!";
		}
	}

	public static String replaceColors(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

}
