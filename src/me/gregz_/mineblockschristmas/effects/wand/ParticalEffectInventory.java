package me.gregz_.mineblockschristmas.effects.wand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.gregz_.mineblockschristmas.MineblocksChristmas;
import me.gregz_.mineblockschristmas.chat.Logger;
import me.gregz_.mineblockschristmas.commands.permission.Permission;
import me.gregz_.mineblockschristmas.commands.permission.PermissionHandler;
import me.gregz_.mineblockschristmas.utils.GlowEffect;
import me.gregz_.mineblockschristmas.utils.Util;

public class ParticalEffectInventory {

	public ParticalEffectInventory() {
		reload();
	}

	public HashMap<UUID, ParticalEffectEnum> selectedParticalEffect;
	public HashMap<UUID, Long> effectUseCooldown;

	public String inventoryName;
	public Inventory inventory;

	private FileConfiguration particleeffects;

	private ItemStack windOfBloodItem; // Wind of blood (slot 0)
	private ItemStack magicWaveItem; // Magic Wave (slot 1)
	private ItemStack fireWaveItem; // Fire Wave (slot 2)
	private ItemStack waterWaveItem; // Water Wave (slot 3)
	private ItemStack sparkCannonItem; // Spark Cannon (slot 4)
	private ItemStack lavaPopCannon; // Lava Pop Cannon (slot 5)
	private ItemStack randomWaveItem; // Power Wave (slot 6)
	private ItemStack fireBallCannonItem; // Fire Ball (slot 7)
	private ItemStack helixOfRandomItem; // Helix of power (slot 8)

	private long standardCooldown;
	private long shortCooldown;
	private List<String> noPermission;

	public ItemStack wand;
	public int slot;

	public void reload() {
		selectedParticalEffect = new HashMap<>();
		effectUseCooldown = new HashMap<>();

		particleeffects = MineblocksChristmas.particleeffects;

		inventoryName = ChatColor.translateAlternateColorCodes('&', particleeffects.getString("InventoryTitle", "&dPartical Effects!"));
		if (inventoryName.length() > 32) {
			inventoryName = inventoryName.substring(0, 32);
		}
		inventory = Bukkit.getServer().createInventory(null, 9, inventoryName);

		windOfBloodItem = createItem(Material.REDSTONE, 1, particleeffects.getString("SelectorItemNames.HELIXOFRANDOM", "&cWind Of Blood"), null);
		magicWaveItem = createItem(Material.CAKE, 1, particleeffects.getString("SelectorItemNames.MAGICWAVE", "&dMagic Wave"), null);
		fireWaveItem = createItem(Material.FLINT_AND_STEEL, 1, particleeffects.getString("SelectorItemNames.FIREWAVE", "&6Fire Wave"), null);
		waterWaveItem = createItem(Material.WATER_BUCKET, 1, particleeffects.getString("SelectorItemNames.WATERWAVE", "&9Water Wave"), null);
		sparkCannonItem = createItem(Material.FIREWORK, 1, particleeffects.getString("SelectorItemNames.SPARKCANNON", "&3Spark Cannon"), null);
		lavaPopCannon = createItem(Material.LAVA_BUCKET, 1, particleeffects.getString("SelectorItemNames.LAVAPOPCANNON", "&4&K12&r &cLava Pop Cannon &4&K12"), null);
		randomWaveItem = createItem(Material.DIAMOND, 1, particleeffects.getString("SelectorItemNames.RANDOMWAVE", "&4&K12&r &cWave of Random &4&K12"), null);
		fireBallCannonItem = createItem(Material.FIREBALL, 1, particleeffects.getString("SelectorItemNames.FIREBALLCANNON", "&6Fire Ball Cannon"), null);
		helixOfRandomItem = createItem(Material.EMERALD, 1, particleeffects.getString("SelectorItemNames.HELIXOFRANDOM", "&4&K12&r &cHelix of Random &4&K12"), null);

		setItems();

		standardCooldown = particleeffects.getLong("Cooldown.Standard", 20l);
		shortCooldown = particleeffects.getLong("Cooldown.Short", 10l);

		noPermission = new ArrayList<>();
		noPermission = particleeffects.getStringList("NoPermissionLore");
		if (noPermission.isEmpty()) {
			noPermission.add("&cYou do not have the required permission to use this effect!");
		}
		for (int i = 0; i < noPermission.size(); i++) {
			noPermission.set(i, ChatColor.translateAlternateColorCodes('&', noPermission.get(i)));
		}

		wand = Util.parseItemStack(particleeffects.getString("Selector.Item", "BLAZE_ROD 1 name:&5&k12&r&d_Effect_Selector_&5&k12 lore:&5&k12&r&d_Right_Click_to_select_an_effect_to_use!_&5&k12"));
		wand = GlowEffect.addGlow(wand);
		slot = particleeffects.getInt("Selector.Slot", 0);
	}

	private void setItems() {
		inventory.setItem(0, windOfBloodItem);
		inventory.setItem(1, magicWaveItem);
		inventory.setItem(2, fireWaveItem);
		inventory.setItem(3, waterWaveItem);
		inventory.setItem(4, sparkCannonItem);
		inventory.setItem(5, lavaPopCannon);
		inventory.setItem(6, randomWaveItem);
		inventory.setItem(7, fireBallCannonItem);
		inventory.setItem(8, helixOfRandomItem);
	}

	private ItemStack createItem(Material material, int ammount, String name, String[] lores) {
		ItemStack item = new ItemStack(material, ammount);
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		if (lores != null) {
			for (String lore : lores) {
				lore = ChatColor.translateAlternateColorCodes('&', lore);
				im.getLore().add(lore);
			}
		}
		item.setItemMeta(im);
		return item;
	}

	public void openEffectSelector(Player p) {
		Inventory temp = inventory;
		if (PermissionHandler.hasPermission(p, Permission.USEALLEFFECTS)) {
			temp = displaySelectedEffect(temp, p);
			p.openInventory(temp);
			return;
		}

		if (!(PermissionHandler.hasPermission(p, Permission.WINDOFBLOODEFFECT))) {
			temp.setItem(0, setNoPermsLore(temp.getItem(0)));
		}
		if (!(PermissionHandler.hasPermission(p, Permission.MAGICWAVE))) {
			temp.setItem(1, setNoPermsLore(temp.getItem(1)));
		}
		if (!(PermissionHandler.hasPermission(p, Permission.FIREWAVE))) {
			temp.setItem(2, setNoPermsLore(temp.getItem(2)));
		}
		if (!(PermissionHandler.hasPermission(p, Permission.WATERWAVE))) {
			temp.setItem(3, setNoPermsLore(temp.getItem(3)));
		}
		if (!(PermissionHandler.hasPermission(p, Permission.SPARKCANNON))) {
			temp.setItem(4, setNoPermsLore(temp.getItem(4)));
		}
		if (!(PermissionHandler.hasPermission(p, Permission.LAVAPOPCANNON))) {
			temp.setItem(5, setNoPermsLore(temp.getItem(5)));
		}
		if (!(PermissionHandler.hasPermission(p, Permission.RANDOMWAVE))) {
			temp.setItem(6, setNoPermsLore(temp.getItem(6)));
		}
		if (!(PermissionHandler.hasPermission(p, Permission.FIREBALLCANNON))) {
			temp.setItem(7, setNoPermsLore(temp.getItem(7)));
		}
		if (!(PermissionHandler.hasPermission(p, Permission.HELIXOFPOWER))) {
			temp.setItem(8, setNoPermsLore(temp.getItem(8)));
		}
		temp = displaySelectedEffect(temp, p);
		p.openInventory(temp);
	}
	
	public ItemStack setNoPermsLore(ItemStack item) {
		ItemMeta im = item.getItemMeta();
		im.setLore(noPermission);
		item.setItemMeta(im);
		return item;
	}

	public Inventory displaySelectedEffect(Inventory inv, Player p) {
		if (!(selectedParticalEffect.containsKey(p.getUniqueId()))) {
			return inv;
		}
		for (int i = 0; i < inv.getSize(); i++) {
			if (inv.getItem(i).containsEnchantment(GlowEffect.getGlow())) {
				inv.getItem(i).removeEnchantment(GlowEffect.getGlow());
			}

		}
		ParticalEffectEnum selectedEffect = selectedParticalEffect.get(p.getUniqueId());
		switch (selectedEffect) {
		case WINDOFBLOODEFFECT:
			inv.setItem(0, GlowEffect.addGlow(inv.getItem(0)));
			break;
		case MAGICWAVE:
			inv.setItem(1, GlowEffect.addGlow(inv.getItem(1)));
			break;
		case FIREWAVE:
			inv.setItem(2, GlowEffect.addGlow(inv.getItem(2)));
			break;
		case WATERWAVE:
			inv.setItem(3, GlowEffect.addGlow(inv.getItem(3)));
			break;
		case SPARKCANNON:
			inv.setItem(4, GlowEffect.addGlow(inv.getItem(4)));
			break;
		case LAVAPOPCANNON:
			inv.setItem(5, GlowEffect.addGlow(inv.getItem(5)));
			break;
		case RANDOMWAVE:
			inv.setItem(6, GlowEffect.addGlow(inv.getItem(6)));
			break;
		case FIREBALLCANNON:
			inv.setItem(7, GlowEffect.addGlow(inv.getItem(7)));
			break;
		case HELIXOFRANDOM:
			inv.setItem(8, GlowEffect.addGlow(inv.getItem(8)));
			break;
		}
		return inv;
	}

	// COOLDOWN
	private long getCooldownTime(Player p) {
		if (PermissionHandler.hasPermission(p, Permission.SHORTEFFECTCOOLDOWN)) {
			return shortCooldown;
		}
		return standardCooldown;
	}

	public boolean cooldownCanUse(Player p) {
		if (PermissionHandler.hasPermission(p, Permission.BYPASSEFFECTCOOLDOWN)) {
			return true;
		}
		if (effectUseCooldown.containsKey(p.getUniqueId())) {
			if (effectUseCooldown.get(p.getUniqueId()) * 1000 <= System.currentTimeMillis()) {
				effectUseCooldown.remove(p.getUniqueId());
				return true;
			}
			return false;
		}
		return true;
	}

	public void addToCooldown(Player p) {
		if (PermissionHandler.hasPermission(p, Permission.BYPASSEFFECTCOOLDOWN)) {
			return;
		}
		if (effectUseCooldown.containsKey(p.getUniqueId())) {
			Logger.error("Player: " + p.getName() + " had an attempt made to add them to the cooldown list when it already contained them!");
			Logger.error("To resolve this the players time in the cooldown list has been replaced with the new time.");
			effectUseCooldown.remove(p.getUniqueId());
		}
		effectUseCooldown.put(p.getUniqueId(), System.currentTimeMillis() / 1000 + getCooldownTime(p));
		Util.debug("Player: " + p.getName() + " has been added to the cooldown effect use for " + getCooldownTime(p) + " seconds!");
	}

	public String getRemainingCooldownTime(Player p) {
		if (!(effectUseCooldown.containsKey(p.getUniqueId()))) {
			Logger.error("Player " + p.getName() + " Is not in the cooldown for using a partical effect thus no remaining time can be found!");
			return "0 Seconds";
		}
		return Util.getFormatedTime(effectUseCooldown.get(p.getUniqueId()) - System.currentTimeMillis() / 1000);
	}

	// PERMISSIONS
	public boolean hasEffectPermission(Player p, ParticalEffectEnum effect) {
		if (PermissionHandler.hasPermission(p, Permission.USEALLEFFECTS)) {
			return true;
		}
		switch (effect) {
		case WINDOFBLOODEFFECT:
			if (PermissionHandler.hasPermission(p, Permission.WINDOFBLOODEFFECT)) {
				return true;
			}
			return false;
		case MAGICWAVE:
			if (PermissionHandler.hasPermission(p, Permission.MAGICWAVE)) {
				return true;
			}
			return false;
		case FIREWAVE:
			if (PermissionHandler.hasPermission(p, Permission.FIREWAVE)) {
				return true;
			}
			return false;
		case WATERWAVE:
			if (PermissionHandler.hasPermission(p, Permission.WATERWAVE)) {
				return true;
			}
			return false;
		case SPARKCANNON:
			if (PermissionHandler.hasPermission(p, Permission.SPARKCANNON)) {
				return true;
			}
			return false;
		case LAVAPOPCANNON:
			if (PermissionHandler.hasPermission(p, Permission.LAVAPOPCANNON)) {
				return true;
			}
			return false;
		case RANDOMWAVE:
			if (PermissionHandler.hasPermission(p, Permission.RANDOMWAVE)) {
				return true;
			}
			return false;
		case FIREBALLCANNON:
			if (PermissionHandler.hasPermission(p, Permission.FIREBALLCANNON)) {
				return true;
			}
			return false;
		case HELIXOFRANDOM:
			if (PermissionHandler.hasPermission(p, Permission.HELIXOFPOWER)) {
				return true;
			}
			return false;
		default:
			return false;
		}
	}

	// SELECT EFFECT
	public boolean selectEffect(Player p, int slot) {
		switch (slot) {
		case 0:
			if (hasEffectPermission(p, ParticalEffectEnum.WINDOFBLOODEFFECT)) {
				Util.debug("Player " + p.getName() + " just selected WINDOFBLOODEFFECT!");
				selectedParticalEffect.put(p.getUniqueId(), ParticalEffectEnum.WINDOFBLOODEFFECT);
				return true;
			}
		case 1:
			if (hasEffectPermission(p, ParticalEffectEnum.MAGICWAVE)) {
				Util.debug("Player " + p.getName() + " just selected MAGICWAVE!");
				selectedParticalEffect.put(p.getUniqueId(), ParticalEffectEnum.MAGICWAVE);
				return true;
			}
		case 2:
			if (hasEffectPermission(p, ParticalEffectEnum.FIREWAVE)) {
				Util.debug("Player " + p.getName() + " just selected FIREWAVE!");
				selectedParticalEffect.put(p.getUniqueId(), ParticalEffectEnum.FIREWAVE);
				return true;
			}
		case 3:
			if (hasEffectPermission(p, ParticalEffectEnum.WATERWAVE)) {
				Util.debug("Player " + p.getName() + " just selected WATERWAVE!");
				selectedParticalEffect.put(p.getUniqueId(), ParticalEffectEnum.WATERWAVE);
				return true;
			}
		case 4:
			if (hasEffectPermission(p, ParticalEffectEnum.SPARKCANNON)) {
				Util.debug("Player " + p.getName() + " just selected SPARKCANNON!");
				selectedParticalEffect.put(p.getUniqueId(), ParticalEffectEnum.SPARKCANNON);
				return true;
			}
		case 5:
			if (hasEffectPermission(p, ParticalEffectEnum.LAVAPOPCANNON)) {
				Util.debug("Player " + p.getName() + " just selected LAVAPOPCANNON!");
				selectedParticalEffect.put(p.getUniqueId(), ParticalEffectEnum.LAVAPOPCANNON);
				return true;
			}
		case 6:
			if (hasEffectPermission(p, ParticalEffectEnum.RANDOMWAVE)) {
				Util.debug("Player " + p.getName() + " just selected RANDOMWAVE!");
				selectedParticalEffect.put(p.getUniqueId(), ParticalEffectEnum.RANDOMWAVE);
				return true;
			}
		case 7:
			if (hasEffectPermission(p, ParticalEffectEnum.FIREBALLCANNON)) {
				Util.debug("Player " + p.getName() + " just selected FIREBALLCANNON!");
				selectedParticalEffect.put(p.getUniqueId(), ParticalEffectEnum.FIREBALLCANNON);
				return true;
			}
		case 8:
			if (hasEffectPermission(p, ParticalEffectEnum.HELIXOFRANDOM)) {
				Util.debug("Player " + p.getName() + " just selected HELIXOFRANDOM!");
				selectedParticalEffect.put(p.getUniqueId(), ParticalEffectEnum.HELIXOFRANDOM);
				return true;
			}
		default:
			return false;
		}
	}
}
