package me.gregz_.mineblockschristmas.effects.wand;

public enum ParticalEffectEnum {
	
	WINDOFBLOODEFFECT,
	
	MAGICWAVE,
	
	FIREWAVE,
	
	WATERWAVE,
	
	SPARKCANNON,
	
	LAVAPOPCANNON,
	
	RANDOMWAVE,
	
	FIREBALLCANNON,
	
	HELIXOFRANDOM;
	
}
