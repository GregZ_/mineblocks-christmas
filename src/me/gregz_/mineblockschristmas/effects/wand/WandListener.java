package me.gregz_.mineblockschristmas.effects.wand;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import me.gregz_.mineblockschristmas.MineblocksChristmas;
import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import me.gregz_.mineblockschristmas.commands.permission.Permission;
import me.gregz_.mineblockschristmas.commands.permission.PermissionHandler;
import me.gregz_.mineblockschristmas.utils.Util;

public class WandListener implements Listener {
	
	@EventHandler
	public void onWandDrop(PlayerDropItemEvent e) {
		if (e.getItemDrop().getItemStack().equals(MineblocksChristmas.particalEffectInventory.wand)) {
			if (PermissionHandler.hasPermission(e.getPlayer(), Permission.DROPWANDITEM)) {
				if (!(e.getPlayer().isSneaking())) {
					e.getPlayer().sendMessage(MessageHandler.getMessage("prefix") + "�cYou must be sneaking to drop the effect wand!");
					Util.debug("Player " + e.getPlayer().getName() + " tried to drop the effect wand but was not sneaking!");
					e.setCancelled(true);
				}
				return;
			}
			e.setCancelled(true);
			Util.debug("Player " + e.getPlayer().getName() + " tried to drop the effect wand but does not have the permission 'mbc.dropwand'!");
		}
	}
	
	@EventHandler
	public void onEffectSelect(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (e.getClickedInventory().getName() == MineblocksChristmas.particalEffectInventory.inventoryName) {
			e.setCancelled(true);
			if (MineblocksChristmas.particalEffectInventory.selectEffect(p, e.getSlot())) {
				p.closeInventory();
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
				return;
			}
			p.playSound(p.getLocation(), Sound.VILLAGER_NO, 1, 1);
		}
	}

	@EventHandler
	public void onWandClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
			Player p = e.getPlayer();
			ItemStack hand = p.getItemInHand();
			if (hand.equals(MineblocksChristmas.particalEffectInventory.wand)) {
				e.setCancelled(true);
				if (MineblocksChristmas.particalEffectInventory.selectedParticalEffect.get(p.getUniqueId()) == null) {
					p.sendMessage(MessageHandler.getMessage("effect-not-selected"));
					return;
				}
				if (!(MineblocksChristmas.particalEffectInventory.cooldownCanUse(p))) {
					p.sendMessage(MessageHandler.getMessage("effect-on-cooldown").replace("%0%", MineblocksChristmas.particalEffectInventory.getRemainingCooldownTime(p)));
					return;
				}
				if (MineblocksChristmas.particalEffectInventory.hasEffectPermission(p, MineblocksChristmas.particalEffectInventory.selectedParticalEffect.get(p.getUniqueId()))) {
					MineblocksChristmas.particalEffectManager.playEffect(p, MineblocksChristmas.particalEffectInventory.selectedParticalEffect.get(p.getUniqueId()));
					MineblocksChristmas.particalEffectInventory.addToCooldown(p);
					return;
				} else {
					p.sendMessage(MessageHandler.getMessage("prefix") + "�cYou do not have permission to use the selected particle effect so your selection has been reset!");
					MineblocksChristmas.particalEffectInventory.selectedParticalEffect.remove(p.getUniqueId());
					return;
				}
			}
		}
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Player p = e.getPlayer();
			ItemStack hand = p.getItemInHand();
			if (hand.equals(MineblocksChristmas.particalEffectInventory.wand)) {
				MineblocksChristmas.particalEffectInventory.openEffectSelector(p);
				return;
			}
		}
	}

	@EventHandler
	public void playerJoin(final PlayerJoinEvent e) {
		new BukkitRunnable() {
            @Override
            public void run() {
            	e.getPlayer().getInventory().setItem(MineblocksChristmas.particalEffectInventory.slot, MineblocksChristmas.particalEffectInventory.wand);
            }
        }.runTaskLater(MineblocksChristmas.instance, 2);
	}
	
	@EventHandler
	public void playerQuit(PlayerQuitEvent e) {
		if (MineblocksChristmas.particalEffectInventory.selectedParticalEffect.containsKey(e.getPlayer().getUniqueId())) {
			MineblocksChristmas.particalEffectInventory.selectedParticalEffect.remove(e.getPlayer().getUniqueId());
		}
	}
}
