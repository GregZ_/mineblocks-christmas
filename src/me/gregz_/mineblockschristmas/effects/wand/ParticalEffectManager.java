package me.gregz_.mineblockschristmas.effects.wand;

import java.util.Random;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import me.gregz_.mineblockschristmas.MineblocksChristmas;

public class ParticalEffectManager {

	public ParticalEffectManager() {
		// CONSTRUCTOR
	}

	public void playEffect(Player p, ParticalEffectEnum effect) {
		switch (effect) {
			case WINDOFBLOODEFFECT:
				windOfBloodEffect(p);
				break;
			case MAGICWAVE:
				waveOfMagicEffect(p);
				break;
			case FIREWAVE:
				waveOfFireEffect(p);
				break;
			case WATERWAVE:
				waveOfWaterEffect(p);
				break;
			case SPARKCANNON:
				sparkConnonEffect(p);
				break;
			case LAVAPOPCANNON:
				lavaPopConnonEffect(p);
				break;
			case RANDOMWAVE:
				waveOfRandomEffect(p);
				break;
			case FIREBALLCANNON:
				fireBallEffect(p);
				break;
			case HELIXOFRANDOM:
				helixOfPowerEffect(p);
				break;
		}
	}
	
	private void windOfBloodEffect(final Player p) {
		new BukkitRunnable() {
			double phi = 0;

			public void run() {
				phi = phi + Math.PI / 8;
				double x, y, z;

				Location loc = p.getLocation();
				for (double t = 0; t <= 2 * Math.PI; t = t + Math.PI / 16) {
					for (double i = 0; i <= 1; i = i + 1) {
						x = 0.4 * (2 * Math.PI - t) * 0.5 * Math.cos(t + phi + i * Math.PI);
						y = 0.5 * t;
						z = 0.4 * (2 * Math.PI - t) * 0.5 * Math.sin(t + phi + i * Math.PI);
						loc.add(x, y, z);

						p.getWorld().spigot().playEffect(loc, Effect.COLOURED_DUST, 0, 0, 0, 0, 0, 0, 1, 100);

						loc.subtract(x, y, z);
					}
				}

				if (phi > 10 * Math.PI) {
					this.cancel();
				}
			}
		}.runTaskTimer(MineblocksChristmas.instance, 0, 2);
	}

	private void waveOfMagicEffect(final Player p) {
		new BukkitRunnable() {
			double t = Math.PI / 4;
			Location loc = p.getLocation();

			public void run() {
				t = t + 0.1 * Math.PI;
				for (double theta = 0; theta <= 2 * Math.PI; theta = theta + Math.PI / 64) {
					double x = t * Math.cos(theta);
					double y = Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
					double z = t * Math.sin(theta);
					loc.add(x, y, z);

					p.getWorld().spigot().playEffect(loc, Effect.HAPPY_VILLAGER, 0, 0, 0, 0, 0, 0, 1, 100);

					loc.subtract(x, y, z);

					theta = theta + Math.PI / 128;

					x = t * Math.cos(theta);
					y = Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
					z = t * Math.sin(theta);
					loc.add(x, y, z);

					p.getWorld().spigot().playEffect(loc, Effect.WITCH_MAGIC, 0, 0, 0, 0, 0, 0, 1, 100);

					loc.subtract(x, y, z);
				}
				if (t > 20) {
					this.cancel();
				}
			}
		}.runTaskTimer(MineblocksChristmas.instance, 1, 1);
	}

	private void waveOfFireEffect(final Player p) {
		new BukkitRunnable() {
			double t = Math.PI / 4;
			Location loc = p.getLocation();

			public void run() {
				t = t + 0.1 * Math.PI;
				for (double theta = 0; theta <= 2 * Math.PI; theta = theta + Math.PI / 64) {
					double x = t * Math.cos(theta);
					double y = Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
					double z = t * Math.sin(theta);
					loc.add(x, y, z);

					p.getWorld().spigot().playEffect(loc, Effect.FLAME, 0, 0, 0, 0, 0, 0, 1, 100);

					loc.subtract(x, y, z);

					theta = theta + Math.PI / 128;

					x = t * Math.cos(theta);
					y = Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
					z = t * Math.sin(theta);
					loc.add(x, y, z);

					p.getWorld().spigot().playEffect(loc, Effect.PARTICLE_SMOKE, 0, 0, 0, 0, 0, 0, 1, 100);

					loc.subtract(x, y, z);
				}
				if (t > 20) {
					this.cancel();
				}
			}
		}.runTaskTimer(MineblocksChristmas.instance, 1, 1);
	}

	private void waveOfWaterEffect(final Player p) {
		new BukkitRunnable() {
			double t = Math.PI / 4;
			Location loc = p.getLocation();

			public void run() {
				t = t + 0.1 * Math.PI;
				for (double theta = 0; theta <= 2 * Math.PI; theta = theta + Math.PI / 64) {
					double x = t * Math.cos(theta);
					double y = Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
					double z = t * Math.sin(theta);
					loc.add(x, y, z);

					p.getWorld().spigot().playEffect(loc, Effect.SPLASH, 0, 0, 0, 0, 0, 0, 1, 100);

					loc.subtract(x, y, z);

					theta = theta + Math.PI / 128;

					x = t * Math.cos(theta);
					y = Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
					z = t * Math.sin(theta);
					loc.add(x, y, z);

					p.getWorld().spigot().playEffect(loc, Effect.MAGIC_CRIT, 0, 0, 0, 0, 0, 0, 1, 100);

					loc.subtract(x, y, z);
				}
				if (t > 20) {
					this.cancel();
				}
			}
		}.runTaskTimer(MineblocksChristmas.instance, 1, 1);
	}

	private void sparkConnonEffect(final Player p) {
		new BukkitRunnable() {
			double t = 0;
			Location loc = p.getLocation();
			Vector direction = loc.getDirection().normalize();

			public void run() {
				t = t + 0.25;
				double x = direction.getX() * t;
				double y = direction.getY() * t + 1.5;
				double z = direction.getZ() * t;
				loc.add(x, y, z);

				p.getWorld().spigot().playEffect(loc, randomParticle(), 0, 0, 0, 0, 0, 0, 1, 100);

				if (loc.getWorld().getBlockAt(loc).getType() != Material.AIR) {
					p.getWorld().spigot().playEffect(loc, Effect.FLYING_GLYPH, 0, 0, 1, 1, 1, 2, 90, 100);
					p.getWorld().playSound(loc, Sound.PORTAL, 2, 1);
					this.cancel();
				}
				
				loc.subtract(x, y, z);

				if (t > 180) {
					this.cancel();
				}
			}
		}.runTaskTimer(MineblocksChristmas.instance, 0, 1);
	}

	private void lavaPopConnonEffect(final Player p) {
		new BukkitRunnable() {
			double t = 0;
			Location loc = p.getLocation();
			Vector direction = loc.getDirection().normalize();

			public void run() {
				t = t + 0.5;
				double x = direction.getX() * t;
				double y = direction.getY() * t + 1.5;
				double z = direction.getZ() * t;
				loc.add(x, y, z);

				p.getWorld().spigot().playEffect(loc, Effect.LAVA_POP, 0, 0, 0, 0, 0, 0, 8, 100);
				
				if (loc.getWorld().getBlockAt(loc).getType() != Material.AIR) {
					p.getWorld().spigot().playEffect(loc, Effect.EXPLOSION_LARGE, 0, 0, 1, 1, 1, 1, 3, 100);
					p.getWorld().playSound(loc, Sound.EXPLODE, 5, 0);
					this.cancel();
				}

				loc.subtract(x, y, z);

				if (t > 180) {
					this.cancel();
				}
			}
		}.runTaskTimer(MineblocksChristmas.instance, 0, 1);
	}

	private void waveOfRandomEffect(final Player p) {
		p.getLocation().getWorld().strikeLightningEffect(p.getLocation());
		new BukkitRunnable() {
			double t = Math.PI / 4;
			Location loc = p.getLocation();

			public void run() {
				t = t + 0.1 * Math.PI;
				for (double theta = 0; theta <= 2 * Math.PI; theta = theta + Math.PI / 64) {
					double x = t * Math.cos(theta);
					double y = Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
					double z = t * Math.sin(theta);
					loc.add(x, y, z);

					p.getWorld().spigot().playEffect(loc, randomParticle(), 0, 0, 0, 0, 0, 0, 1, 100);

					loc.subtract(x, y, z);

					theta = theta + Math.PI / 128;

					x = t * Math.cos(theta);
					y = Math.exp(-0.1 * t) * Math.sin(t) + 1.5;
					z = t * Math.sin(theta);
					loc.add(x, y, z);

					p.getWorld().spigot().playEffect(loc, randomParticle(), 0, 0, 0, 0, 0, 0, 1, 100);

					loc.subtract(x, y, z);
				}
				if (t > 20) {
					this.cancel();
				}
			}
		}.runTaskTimer(MineblocksChristmas.instance, 1, 1);
	}

	private void fireBallEffect(final Player p) {
		p.getLocation().getWorld().playSound(p.getLocation(), Sound.GHAST_FIREBALL, 10, 1);
		final Fireball fb = p.launchProjectile(Fireball.class);
		fb.setIsIncendiary(false);
		fb.setYield(0);
		new BukkitRunnable() {

			public void run() {
				if (fb.isDead()) {
					this.cancel();
				}
				fb.getLocation().getWorld().spigot().playEffect(fb.getLocation(), Effect.LAVA_POP, 0, 0, 0.2f, 0.2f, 0.2f, 1, 8, 100);
			}
		}.runTaskTimer(MineblocksChristmas.instance, 1, 1);
	}

	private void helixOfPowerEffect(final Player p) {
		new BukkitRunnable() {
			double t = 0;

			public void run() {
				t = t + 0.5;
				Location loc = p.getLocation();
				int radius = 2;
				int maxHeight = 10;
				for (double y = 0; y <= maxHeight; y += 0.05) {
					double x = radius * Math.cos(y);
					double z = radius * Math.sin(y);

					p.getWorld().spigot().playEffect(new Location(loc.getWorld(), loc.getX() + x, loc.getY() + y, loc.getZ() + z), randomParticle(), 0, 0, 0, 0, 0, 0, 1, 100);

				}
				if (t > 90) {
					this.cancel();
				}
			}
		}.runTaskTimer(MineblocksChristmas.instance, 1, 1);
	}

	private Effect randomParticle() {
		Random r = new Random();
		int rNumb = r.nextInt(5);
		if (rNumb == 1) {
			return Effect.MAGIC_CRIT;
		} else if (rNumb == 2) {
			return Effect.WITCH_MAGIC;
		} else if (rNumb == 3) {
			return Effect.FLAME;
		} else if (rNumb == 4) {
			return Effect.SPLASH;
		} else if (rNumb == 5) {
			return Effect.PORTAL;
		} else {
			return Effect.HAPPY_VILLAGER;
		}
	}
}
