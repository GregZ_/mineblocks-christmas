package me.gregz_.mineblockschristmas.effects.snow;

import java.util.Random;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import me.gregz_.mineblockschristmas.MineblocksChristmas;

public class SnowHub {
	private Location centre;
	private int range;
	private int minAmount;
	private int maxAmount;
	private String name;

	private BukkitRunnable snow;
	private boolean running;
	private Random r = new Random();

	public SnowHub(String name, Location center, int range, int minAmount, int maxAmount) {
		this.name = name;
		this.centre = center;
		this.range = range;
		this.minAmount = minAmount;
		this.maxAmount = maxAmount;
		startSnow();
	}

	public void startSnow() {
		if (running) {
			return;
		}
		running = true;
		snow = new BukkitRunnable() {
			int n;

			@Override
			public void run() {
				for (int i = 0; i < 8; ++i) {
					int partAmnt = Math.min(minAmount, maxAmount) + ((n = Math.abs(maxAmount - minAmount)) == 0 ? 0 : r.nextInt(n));
					float radiusY = (10.0f + (70.0f - 10.0f) * r.nextFloat());
					centre.getWorld().spigot().playEffect(centre, Effect.FIREWORKS_SPARK, 0, 0, range, radiusY, range, 0, partAmnt, range);
				}
				if (!(running)) {
					this.cancel();
				}
			}
		};
		snow.runTaskTimer(MineblocksChristmas.instance, 0, 40);

	}

	public boolean stopSnow() {
		if (running) {
			snow.cancel();
			running = false;
			return true;
		}
		return false;
	}

	public boolean isRunning() {
		return running;
	}

	public String getName() {
		return name;
	}
}
