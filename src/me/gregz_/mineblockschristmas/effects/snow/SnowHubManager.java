package me.gregz_.mineblockschristmas.effects.snow;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.gregz_.mineblockschristmas.MineblocksChristmas;
import me.gregz_.mineblockschristmas.chat.Logger;
import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import me.gregz_.mineblockschristmas.utils.Util;

public class SnowHubManager {

	private List<SnowHub> snowHubs;
	private FileConfiguration database;
	private FileConfiguration cfg;
	private int minSnow;
	private int maxSnow;
	private int range;

	public SnowHubManager() {
		reinitializeConfig();
		reinitializeDatabase();
	}

	public void reinitializeConfig() {
		cfg = MineblocksChristmas.instance.getConfig();
		minSnow = cfg.getInt("Defaults.Snow.Amount.Min", 1250);
		maxSnow = cfg.getInt("Defaults.Snow.Amount.Max", 1500);
		range = cfg.getInt("Defaults.Snow.Range", 100);
	}

	public void reinitializeDatabase() {
		database = MineblocksChristmas.database;
		snowHubs = new ArrayList<>();
		loadAll();
	}

	public void add(Player p, String name) {
		String path = ("SnowHubs." + name);
		if (database.contains(path)) {
			p.sendMessage(MessageHandler.getMessage("snowhub-already-exists").replace("%0%", name));
			if (getSnowHub(name) == null) {
				p.sendMessage(MessageHandler.getMessage("prefix") + "Snow hub " + name + " will thus be loaded!");
				load(name);
				return;
			}
			p.sendMessage(MessageHandler.getMessage("prefix") + "If you wish to use this name please delete the Snow hub named " + name + " or move it to your current location!");
		}

		Location loc = p.getLocation();
		String sloc = Util.serializeLocation(loc, false);

		path += ".";

		database.set(path + "Center", sloc);
		database.set(path + "Range", range);
		database.set(path + "MinSnow", minSnow);
		database.set(path + "MaxSnow", maxSnow);

		MineblocksChristmas.saveDataBase();

		snowHubs.add(new SnowHub(name, loc, range, minSnow, maxSnow));

		p.sendMessage(MessageHandler.getMessage("snowhub-created").replace("%0%", name));
		Logger.log("Player " + p.getName() + " successfully created and loaded the Snow Hub " + name + "!");
	}

	public void move(Player p, String name) {
		if (getSnowHub(name) != null) {
			p.sendMessage(MessageHandler.getMessage("prefix") + "�cYou must unload the snow hub first! /mbc snow unload " + name);
			return;
		}
		String path = "SnowHubs." + name;
		if (!(database.contains(path))) {
			p.sendMessage(MessageHandler.getMessage("snowhub-not-found").replace("%0%", name));
			return;
		}
		database.set(path + ".Center", Util.serializeLocation(p.getLocation(), false));
		p.sendMessage(MessageHandler.getMessage("snowhub-move-success").replace("%0%", name));
		Logger.log("Player " + p.getName() + " successfully moved the snow hub " + name + " to there current location!");
	}
	
	public void reload(Player p, String name) {
		if (getSnowHub(name) == null) {
			p.sendMessage(MessageHandler.getMessage("prefix") + "Snow hub " + name + " is not loaded!");
			return;
		}
		if (!(database.contains("SnowHubs." + name))) {
			p.sendMessage(MessageHandler.getMessage("snowhub-not-found").replace("%0%", name));
			return;
		}
		
		if (!(unload(name))) {
			p.sendMessage(MessageHandler.getMessage("prefix") + "�cError unloading snow hub " + name + " please view the console for details!");
		}
		if (!(load(name))) {
			p.sendMessage(MessageHandler.getMessage("prefix") + "�cError loading snow hub " + name + " please view the console for details!");
		}
		
		p.sendMessage(MessageHandler.getMessage("snowhub-reload-success").replace("%0%", name));
		Logger.log("Player " + p.getName() + " successfully reloaded the snow hub " + name + "!");
	}
	
	public void reloadAll(Player p) {
		int reloaded = 0;
		for (SnowHub snowHub : snowHubs) {
			unload(snowHub.getName());
			load(snowHub.getName());
			reloaded ++;
		}
		p.sendMessage(MessageHandler.getMessage("prefix") + reloaded + " snow " + (reloaded == 1 ? "hub" : "hubs") + " reloaded!");
		Logger.log(p.getName() + " reloaded " + reloaded + " snow " + (reloaded == 1 ? "hub" : "hubs") + "!");
	}
	
	public void delete(Player p, String name) {
		String path = ("SnowHubs." + name);
		if (!(database.contains(path))) {
			p.sendMessage(MessageHandler.getMessage("prefix") + "Snow hub " + name + " does not exist!");
			return;
		}

		if (getSnowHub(name) != null) {
			SnowHub snowHub = getSnowHub(name);
			snowHub.stopSnow();
			snowHubs.remove(snowHub);
			p.sendMessage(MessageHandler.getMessage("prefix") + "Unloaded Snow hub " + name + "!");
		}
		
		database.set(path, null);
		p.sendMessage(MessageHandler.getMessage("prefix") + "Successfully removed Snow Hub " + name + "!");
		Logger.log("Player " + p.getName() + " successfully removed Snow Hub " + name + "!");
	}
	
	public void loadAll() {
		int loaded = 0;

		if (database.contains("SnowHubs")) {
			for (String key : database.getConfigurationSection("SnowHubs.").getKeys(false)) {
				if (load(key))
					loaded++;
			}
		}

		Logger.log(loaded + " snow " + (loaded == 1 ? "hub" : "hubs") + " loaded!");
	}

	public boolean load(String name) {
		if (getSnowHub(name) != null) {
			Logger.error("Snow hub " + name + " is already loaded!");
			return false;
		}

		String path = "SnowHubs." + name;

		if (!database.contains(path)) {
			Logger.error("Snow hub " + name + " does not exist!");
			return false;
		}

		path += ".";

		if (!database.contains(path + "Center")) {
			Logger.error("Snow hub " + name + " has no center location!");
			return false;
		}

		Location center = Util.parseLocation(database.getString(path + "Center"));

		if (center == null) {
			Logger.error("Error loading center location from string for snow hub " + name + "!");
			return false;
		}
		
		int range = this.range;
		int minSnow = this.minSnow;
		int maxSnow = this.maxSnow;

		if (database.contains(path + "Range")) {
			range = database.getInt(path + "Range");
		} else {
			Logger.error("Snow hub " + name + " has no range set using default range!");
		}

		if (database.contains(path + "MinSnow")) {
			minSnow = database.getInt(path + "MinSnow");
		} else {
			Logger.error("Snow hub " + name + " has no min snow value set using default min snow value!");
		}

		if (database.contains(path + "MaxSnow")) {
			maxSnow = database.getInt(path + "MaxSnow");
		} else {
			Logger.error("Snow hub " + name + " has no min snow value set using default min snow value!");
		}
		snowHubs.add(new SnowHub(name, center, range, minSnow, maxSnow));
		return true;
	}

	public boolean unload(String name) {
		if (getSnowHub(name) == null) {
			Logger.error("Snow Hub " + name + " was attemped to be unloaded when it was not loaded!");
			return false;
		}
		SnowHub snowhub = getSnowHub(name);
		snowhub.stopSnow();
		snowHubs.remove(snowhub);
		return true;
	}
	
	public SnowHub getSnowHub(String name) {
		for (SnowHub snowHub : snowHubs) {
			if (snowHub.getName().equalsIgnoreCase(name))
				return snowHub;
		}
		return null;
	}

	public void stopAllSnow() {
		for (SnowHub snowHub : snowHubs) {
			snowHub.stopSnow();
		}
		snowHubs.clear();
	}

	public void startAllSnow() {
		for (SnowHub snowHub : snowHubs) {
			snowHub.startSnow();
		}
	}
	
	public List<String> getSnowHubs() {
		List<String> snowHubList = new ArrayList<>();
		if (snowHubs.isEmpty()) {
			snowHubList.add("�cNo snow hubs are loaded!");
			return snowHubList;
		}
		StringBuilder sb;
		for (SnowHub snowhub : snowHubs) {
			sb = new StringBuilder();
			sb.append("�7- �6" + snowhub.getName() + ":\n");
			sb.append("    �7- �6Location�8: �e" + database.getString("SnowHubs." + snowhub.getName() + ".Center") + "\n");
			sb.append("    �7- �6Range�8: �e" + database.getInt("SnowHubs." + snowhub.getName() + ".Range", 100) + "\n");
			sb.append("    �7- �6MinSnow�8: �e" + database.getInt("SnowHubs." + snowhub.getName() + ".MinSnow", 1250) + "\n");
			sb.append("    �7- �6MaxSnow�8: �e" + database.getInt("SnowHubs." + snowhub.getName() + ".MaxSnow", 1500));
			snowHubList.add(sb.toString());
			
		}
		return snowHubList;
	}
}
