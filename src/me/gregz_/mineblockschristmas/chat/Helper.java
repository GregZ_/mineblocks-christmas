package me.gregz_.mineblockschristmas.chat;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public class Helper {

	public static void showConfigHelpsite(Player p) {
		p.sendMessage(MessageHandler.getMessage("prefix") + "Configuration �7�m---�r �6Helpsite");
		for(BaseComponent[] bc : config) {
			p.spigot().sendMessage(bc);
		}
	}

	public static void showSnowHelpsite(Player p) {
		p.sendMessage(MessageHandler.getMessage("prefix") + "Snow Hub Management �7�m---�r �6Helpsite");
		for(BaseComponent[] bc : snow) {
			p.spigot().sendMessage(bc);
		}
	}
	
	public static void showBroadcastsHelpsite(Player p) {
		p.sendMessage(MessageHandler.getMessage("prefix") + "Broadcasts Management �7�m---�r �6Helpsite");
		for(BaseComponent[] bc : broadcasts) {
			p.spigot().sendMessage(bc);
		}
	}

	private static List<BaseComponent[]> config, snow, broadcasts;

	static {
		snow = new ArrayList<>();
		snow.add(new ComponentBuilder("�8/�6mcb snow add <SNOW HUB NAME> �7- �eCreates a snow hub at your location with the specified name!").color(ChatColor.YELLOW).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�bClick to prepare command...").create())).event(new ClickEvent(Action.SUGGEST_COMMAND, "/mbc snow add ")).create());
		snow.add(new ComponentBuilder("�8/�6mcb snow delete <SNOW HUB NAME> �7- �eRemoves the snow hub with the specified name!").color(ChatColor.YELLOW).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�bClick to prepare command...").create())).event(new ClickEvent(Action.SUGGEST_COMMAND, "/mbc snow delete ")).create());
		snow.add(new ComponentBuilder("�8/�6mcb snow move <SNOW HUB NAME> �7- �eMoves the snow hub with the specified name tp your location!").color(ChatColor.YELLOW).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�bClick to prepare command...").create())).event(new ClickEvent(Action.SUGGEST_COMMAND, "/mbc snow move ")).create());
		snow.add(new ComponentBuilder("�8/�6mcb snow reload <SNOW HUB NAME> �7- �eReloads the snow hub with the specified name if it exists!").color(ChatColor.YELLOW).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�bClick to prepare command...").create())).event(new ClickEvent(Action.SUGGEST_COMMAND, "/mbc snow reload ")).create());
		snow.add(new ComponentBuilder("�8/�6mcb snow reloadall �7- �eReloads all currently loaded snow hubs!").color(ChatColor.YELLOW).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�bClick to prepare command...").create())).event(new ClickEvent(Action.SUGGEST_COMMAND, "/mbc snow reloadall")).create());
		snow.add(new ComponentBuilder("�8/�6mcb snow load <SNOW HUB NAME> �7- �eLoads the snow hub with the specified name if it exists!").color(ChatColor.YELLOW).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�bClick to prepare command...").create())).event(new ClickEvent(Action.SUGGEST_COMMAND, "/mbc snow load ")).create());
		snow.add(new ComponentBuilder("�8/�6mcb snow unload <SNOW HUB NAME> �7- �eUnloads the snow hub with the specified name if it exists!").color(ChatColor.YELLOW).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�bClick to prepare command...").create())).event(new ClickEvent(Action.SUGGEST_COMMAND, "/mbc snow unload ")).create());
		snow.add(new ComponentBuilder("�8/�6mcb snow list �7- �eLists all currently loaded snow hubs!").color(ChatColor.YELLOW).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�bClick to prepare command...").create())).event(new ClickEvent(Action.SUGGEST_COMMAND, "/mbc snow list")).create());

		config = new ArrayList<>();
		config.add(new ComponentBuilder("�8/�6mcb config reload [MESSAGES/DATABASE/CONFIG/BROADCASTS/PARTICLEEFFECTS] �7- �eReloads the specified config!").color(ChatColor.YELLOW).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�bClick to prepare command...").create())).event(new ClickEvent(Action.SUGGEST_COMMAND, "/mbc config reload ")).create());
	
		broadcasts.add(new ComponentBuilder("").create());
	}
}
