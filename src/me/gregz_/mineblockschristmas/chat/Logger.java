package me.gregz_.mineblockschristmas.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import me.gregz_.mineblockschristmas.MineblocksChristmas;

public class Logger {
	  public static void log(String msg) {
	        msg = ChatColor.translateAlternateColorCodes('&', "&7[&a" + MineblocksChristmas.instance.getName() + "&7]&r&b " + msg);
	        if (!MineblocksChristmas.instance.getConfig().getBoolean("UseColourInConsole", true)) {
	            msg = ChatColor.stripColor(msg);
	        }
	        Bukkit.getConsoleSender().sendMessage(msg);
	    }
	    
	    public static void error(String msg) {
	        msg = ChatColor.translateAlternateColorCodes('&', "&7[&a" + MineblocksChristmas.instance.getName() + "&7] [&4ERROR&7]&r &c" + msg);
	        if (!MineblocksChristmas.instance.getConfig().getBoolean("UseColourInConsole", true)) {
	            msg = ChatColor.stripColor(msg);
	        }
	        Bukkit.getConsoleSender().sendMessage(msg);
	    }
	    
	    public static void debug(String msg) {
	        msg = ChatColor.translateAlternateColorCodes('&', "&7[&a" + MineblocksChristmas.instance.getName() + "&7] [&2DEBUG&7]&r &a" + msg);
	        if (!MineblocksChristmas.instance.getConfig().getBoolean("UseColourInConsole", true)) {
	            msg = ChatColor.stripColor(msg);
	        }
	        Bukkit.getConsoleSender().sendMessage(msg);
	    }
}
