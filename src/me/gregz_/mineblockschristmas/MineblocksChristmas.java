package me.gregz_.mineblockschristmas;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import me.gregz_.mineblockschristmas.broadcasts.BroadcastManager;
import me.gregz_.mineblockschristmas.chat.Logger;
import me.gregz_.mineblockschristmas.commands.CommandMBC;
import me.gregz_.mineblockschristmas.commands.messages.MessageHandler;
import me.gregz_.mineblockschristmas.database.ConfigLoader;
import me.gregz_.mineblockschristmas.effects.snow.SnowHubManager;
import me.gregz_.mineblockschristmas.effects.wand.ParticalEffectInventory;
import me.gregz_.mineblockschristmas.effects.wand.ParticalEffectManager;
import me.gregz_.mineblockschristmas.effects.wand.WandListener;

public class MineblocksChristmas extends JavaPlugin {

	public static MineblocksChristmas instance;
	public static FileConfiguration messages, database, broadcasts, particleeffects;
	public static SnowHubManager snowHubManager;
	public static BroadcastManager broadcastManager;
	public static ParticalEffectManager particalEffectManager;
	public static ParticalEffectInventory particalEffectInventory;

	@Override
	public void onEnable() {
		instance = this;

		new ConfigLoader().load();
		MessageHandler.reload();

		snowHubManager = new SnowHubManager();
		broadcastManager = new BroadcastManager();
		particalEffectManager = new ParticalEffectManager();
		particalEffectInventory = new ParticalEffectInventory();

		Bukkit.getPluginManager().registerEvents(new WandListener(), this);

		getCommand("mbc").setExecutor(new CommandMBC());

		Logger.log("MineBlocks Christmas is now enabled!");
	}

	@Override
	public void onDisable() {
		snowHubManager.stopAllSnow();
		Logger.log("MineBlocks Christmas is now disabled!");
	}

	// FILECONFIGURATION SAVE
	public static void saveMessages() {
		try {
			messages.save(instance.getDataFolder().toString() + File.separator + "messages.yml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveDataBase() {
		try {
			database.save(instance.getDataFolder().toString() + File.separator + "database.yml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveBroadcasts() {
		try {
			broadcasts.save(instance.getDataFolder().toString() + File.separator + "broadcasts.yml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveParticleEffects() {
		try {
			particleeffects.save(instance.getDataFolder().toString() + File.separator + "particleeffects.yml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// API
	public static MineblocksChristmas getInstance() {
		return instance;
	}

	public static SnowHubManager getSnowHubManager() {
		return snowHubManager;
	}

	public static BroadcastManager getBroadcastManager() {
		return broadcastManager;
	}

	public static ParticalEffectManager getparticalEffectManager() {
		return particalEffectManager;
	}

	public static ParticalEffectInventory getParticalEffectInventory() {
		return particalEffectInventory;
	}
}
